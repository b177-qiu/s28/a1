
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=> response.json())
.then((json)=> console.log(json));
/*
4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
*/

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=> response.json())
.then((json)=> console.log(json));

/*
6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.*/

console.log("The item delectus aut autem on the list has a status of false.");

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
			'Content-type': 'application/json'
		},

	body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created To Do List Item',
		userId: 1,

	}),
})
.then((response)=>response.json())
.then((json)=> console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "pending",
		description: "To update my to do list",
		title: 'Updated To Do List',
		status: "Pending",
		id: 201,
		userId: 1
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		dateCompleted: "11/22/2022",
		status: "Completed",
		id: 201,
		userId: 1
	}),

})
.then((response)=> response.json())
.then((json)=> console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: "DELETE"
});

